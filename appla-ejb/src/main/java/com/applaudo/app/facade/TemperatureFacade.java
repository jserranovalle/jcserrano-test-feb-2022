package com.applaudo.app.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.applaudo.app.entidades.Temperature;
import com.applaudo.app.facadeLocal.TemperatureFacadeLocal;

@Stateless
public class TemperatureFacade extends AbstractFacade<Temperature> implements TemperatureFacadeLocal{

	@PersistenceContext(unitName = "ExampleDS")
    private EntityManager em;
	
	public TemperatureFacade() {
		super(Temperature.class);
	}

	@Override
	public void create(List<Temperature> temperature) {
		for(Temperature tmp : temperature) {
			createOne(tmp);
		}
		
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void createOne(Temperature temperature) {
		em.persist(temperature);
		
	}

	@Override
	public List<Temperature> findByWeatherId(Integer weatherId) {
		List<Temperature> temperatureList = new ArrayList<>();
		//try {
			Query queryTemperature=em.createQuery("FROM Temperature WHERE weather.id=:weatherId");
			queryTemperature.setParameter("weatherId", weatherId);
			temperatureList = queryTemperature.getResultList();
		//} catch (Exception e) {
			//logger.error("Error trying to get Temperatures :", e);
		//}
		
		return temperatureList;
	}

	
}

package com.applaudo.app.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.applaudo.app.entidades.Temperature;
import com.applaudo.app.entidades.Weather;
import com.applaudo.app.facadeLocal.WeatherFacadeLocal;

@Stateless
public class WeatherFacade extends AbstractFacade<Weather> implements WeatherFacadeLocal{

	@PersistenceContext(unitName = "ExampleDS")
    private EntityManager em;
	
	public WeatherFacade() {
		super(Weather.class);
	}

	@Override
	public void create(Weather weather) {
		getEntityManager().persist(weather);
	}

	@Override
	public Weather findById(Integer id) {
		//return findById(id);
		Weather weather = null;
		try {
			Query WeatherQuery=em.createQuery("FROM Weather WHERE id=:id");
			WeatherQuery.setParameter("id", id);
			weather = (Weather) WeatherQuery.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return weather;
	}

	@Override
	public List<Weather> findByCity(String city) {
		List<Weather> weatherList = new ArrayList<>();
		
		List<String> cities = new ArrayList<>();
		String[] cit = city.split(",");
		for(String c:cit) {
			cities.add(c.toUpperCase());
		}		
		
		try {
			Query queryWeatherList=em.createQuery("FROM Weather WHERE UPPER(city) in (:cities)");
			queryWeatherList.setParameter("cities", cities);
			weatherList = queryWeatherList.getResultList();
		} catch (Exception e) {
		}
		
		return weatherList;
	}

	@Override
	public List<Weather> findByDate(String date) {
		return null;
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public Weather add(Weather weather) {
		getEntityManager().persist(weather);
		return weather;
	}

	@Override
	public List<Temperature> findByWeatherId(Integer weatherId) {
		List<Temperature> temperatureList = new ArrayList<>();
		try {
			Query queryTemperature=em.createQuery("FROM Temperature WHERE weather.id=:weatherId");
			queryTemperature.setParameter("weatherId", weatherId);
			temperatureList = queryTemperature.getResultList();
		} catch (Exception e) {
			//logger.error("Error trying to get Temperatures :", e);
		}
		
		return temperatureList;
	}

}

package com.applaudo.app.facadeLocal;

import java.util.List;

import javax.ejb.Local;

import com.applaudo.app.entidades.Temperature;
import com.applaudo.app.entidades.Weather;

@Local
public interface TemperatureFacadeLocal {
	public abstract void createOne(Temperature temperature);
	public abstract void create(List<Temperature> temperature);
	public abstract List<Temperature> findByWeatherId(Integer weatherId);
	
}

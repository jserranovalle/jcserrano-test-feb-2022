package com.applaudo.app.facadeLocal;

import java.util.List;

import javax.ejb.Local;

import com.applaudo.app.entidades.Temperature;
import com.applaudo.app.entidades.Weather;

@Local
public interface WeatherFacadeLocal {
	public abstract void create(Weather weather);
	public abstract Weather add(Weather weather);
	public abstract Weather findById(Integer id);
	public abstract List<Weather> findAll();
	public abstract List<Weather> findByCity(String city);
	public abstract List<Weather> findByDate(String date);
	public abstract List<Temperature> findByWeatherId(Integer weatherId);
}

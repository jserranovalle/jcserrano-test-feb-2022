package com.applaudo.app.entidades;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Weather {	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
	
	@Transient
    private String date;
    
	@JsonIgnore
	private Date date_;

    private Float lat;
    private Float lon;
    private String city;
    private String state;

    @Transient
    private List<Double> temperatures;
    
    @JsonIgnore
    @OneToMany(mappedBy = "weather")
    private List<Temperature> temperatureList;
    
    public void setTemperatureObject() {
    	for(Double temperatureDouble:getTemperatures()) {
    		Temperature temperature = new Temperature();
    		temperature.setWeather(this);
    		temperature.setTemperature(temperatureDouble);
    	}
    }
    
    public void getTemperatureDouble() {
    	for(Temperature temperatureObject:getTemperatureList()) {
    		this.temperatureList.add(temperatureObject);
    	}
    }
    
}

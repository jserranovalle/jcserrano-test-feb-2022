
package com.applaudo.app.Utilidades;

import java.net.URL;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Juan.Serrano
 */
public class LoggerUtil {
    
  private URL url = Thread.currentThread().getContextClassLoader().getResource("log4j.xml");
  public static Logger log = null;
  
  public LoggerUtil(Class<?> loggerClass)
  {
    DOMConfigurator.configure(this.url);
    log = Logger.getLogger(loggerClass);
  }
  
  public static LoggerUtil getLogger(Class<?> loggerClass)
  {
    return new LoggerUtil(loggerClass);
  }
  
  public void error(String msg)
  {
    log.error(msg);
  }
  
  public void error(String msg, Throwable ex)
  {
    log.error(msg, ex);
  }
  
  public void debug(String msg)
  {
    log.debug(msg);
  }
  
  public void info(String msg)
  {
    
      log.info(msg);
  }
  
  public boolean isDebugEnabled()
  {
    return log.isDebugEnabled();
  }
  
  public void warn(String msg)
  {
    log.warn(msg);
  }
}

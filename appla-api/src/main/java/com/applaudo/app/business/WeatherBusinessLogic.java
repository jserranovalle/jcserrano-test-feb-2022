package com.applaudo.app.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import com.applaudo.app.entidades.Temperature;
import com.applaudo.app.entidades.Weather;
import com.applaudo.app.service.TempService;
import com.applaudo.app.service.WeatherService;

@Component
public class WeatherBusinessLogic {
	
	@Autowired
	WeatherService weatherService;
	
	@Autowired
	TempService tempService;
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
public ResponseEntity<?> createUser(Weather weather){
	weather.setDate_(stringToDate(weather.getDate()));
	Weather response = weatherService.add(weather);
	addTemperatureObject(weather);
	tempService.create(addTemperatureObject(weather));	
	return new ResponseEntity<>(response, HttpStatus.CREATED);
	}


public ResponseEntity<?> findAll(String city, String sort, String date){
	
	if(city != null) {
		List<Weather> resp = weatherService.findByCity(city);
		if (resp != null && !resp.isEmpty()) {
			List<Weather> data = getWeatherWithTemperatureDouble(resp);
			setSort(data, sort);
			return new ResponseEntity<>(data, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<Weather>>(resp, HttpStatus.NOT_FOUND);
		}
	}
	
	if(date != null) {
		List<Weather> resp = weatherService.findByDate(date);
		if (resp != null && !resp.isEmpty()) {
			List<Weather> data = getWeatherWithTemperatureDouble(resp);
			setSort(data, sort);
			return new ResponseEntity<>(data, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(resp, HttpStatus.NOT_FOUND);
		}
	}
	
	
	List<Weather> dataList = weatherService.findAll();
	List<Weather> data = getWeatherWithTemperatureDouble(dataList);
	setSort(data, sort);
	return new ResponseEntity<>(data, HttpStatus.OK);
	}

private void setSort(List<Weather> weather, String sort) {
	if(sort != null && sort.equalsIgnoreCase("date")) {
		weather.sort((d1, d2) -> d1.getDate_().compareTo(d2.getDate_()));
	}
    if(sort != null && sort.equalsIgnoreCase("-date")) {
    	weather.sort((d1, d2) -> d2.getDate_().compareTo(d1.getDate_()));
	}
}


public ResponseEntity<?> findById(Integer id) {
	Weather resp = weatherService.findById(id);
	if(resp != null) {
		resp.setDate(dateToString(resp.getDate_()));
		
		return new ResponseEntity<Weather>(getWeatherWithTemperatureDouble(resp), HttpStatus.OK);
	}else {
		return new ResponseEntity<Weather>(resp, HttpStatus.NOT_FOUND);
	}	
}

   private List<Weather> getWeatherWithTemperatureDouble(List<Weather> dataList) {   
	   List<Weather> weList = new ArrayList<>();
	   for(Weather weather : dataList) {
			List<Temperature> temperatureList = tempService.getByWeather(weather);
			List<Double> tempDouble= new ArrayList<>();
			for(Temperature temperature : temperatureList) {
				tempDouble.add(temperature.getTemperature());
			}
			weather.setTemperatures(tempDouble);
			weather.setDate(dateToString(weather.getDate_()));
			weList.add(weather);
		}
	   return weList;
   }
   
   private Weather getWeatherWithTemperatureDouble(Weather weather) {
	   List<Double> tmDouble = new ArrayList<>();
	   List<Temperature> tempList = tempService.getByWeather(weather);
	   for(Temperature temp :tempList) {
		   tmDouble.add(temp.getTemperature());
	   }
	   weather.setTemperatures(tmDouble);
	   return weather;
   }
   
 private List<Temperature>  addTemperatureObject(Weather weather) {  
	 
	 List<Temperature> temperatureList = new ArrayList<>();
	 for(Double d: weather.getTemperatures()) {
		 Temperature tmp = new Temperature();
		 tmp.setTemperature(d);
		 Weather w = new Weather();
		 w.setId(weather.getId());
		 tmp.setWeather(w);
		 temperatureList.add(tmp);
	 }
			
		return temperatureList;
   }
 
 private Date stringToDate(String date) {
	 Date newDate = null;
	 try {
		 newDate = sdf.parse(date);
	 }catch(Exception e) {
		 
	 }
	 return newDate;
 }
 
 private String dateToString(Date date) {
	 return sdf.format(date);
 }
}

package com.applaudo.app.service.impl;

import java.util.List;

import javax.ejb.EJB;

import org.springframework.stereotype.Service;

import com.applaudo.app.entidades.Temperature;
import com.applaudo.app.entidades.Weather;
import com.applaudo.app.facadeLocal.TemperatureFacadeLocal;
import com.applaudo.app.service.TempService;

@Service("tempService")
public class TempServiceImpl implements TempService{
	
	@EJB(mappedName = "java:global/appla-ear/appla-ejb/TemperatureFacade!com.applaudo.app.facadeLocal.TemperatureFacadeLocal")
	private TemperatureFacadeLocal temperatureFacadeDAO;

	@Override
	public List<Temperature> getByWeather(Weather weather) {
		//return null;
		return temperatureFacadeDAO.findByWeatherId(weather.getId());
	}

	@Override
	public void createOne(Temperature temperature) {
		temperatureFacadeDAO.createOne(temperature);
	}

	@Override
	public void create(List<Temperature> temperature) {
		temperatureFacadeDAO.create(temperature);
	}

	@Override
	public List<Temperature> findByWeatherId(Integer weatherId) {
		return temperatureFacadeDAO.findByWeatherId(weatherId);
	}

}

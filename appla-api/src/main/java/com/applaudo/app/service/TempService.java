package com.applaudo.app.service;

import java.util.List;

import com.applaudo.app.entidades.Temperature;
import com.applaudo.app.entidades.Weather;

public interface TempService {
	public abstract List<Temperature> getByWeather(Weather weather);
	
	public abstract void createOne(Temperature temperature);
	public abstract void create(List<Temperature> temperature);
	
	public abstract List<Temperature> findByWeatherId(Integer weatherId);
}

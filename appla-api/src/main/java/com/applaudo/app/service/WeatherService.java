package com.applaudo.app.service;

import java.util.List;

import com.applaudo.app.entidades.Weather;
/**
 * 
 * @author juan.serrano
 *
 */
public interface WeatherService {
	public abstract void create(Weather weather);
	public abstract Weather add(Weather weather);
	public abstract Weather findById(Integer id);
	public abstract List<Weather> findAll();
	public abstract List<Weather> findByCity(String city);
	public abstract List<Weather> findByDate(String date);
}

package com.applaudo.app.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.springframework.stereotype.Service;

import com.applaudo.app.entidades.Weather;
import com.applaudo.app.facadeLocal.WeatherFacadeLocal;
import com.applaudo.app.service.WeatherService;
/**
 * 
 * @author juan.serrano
 *
 */

@Service("weatherService")
public class WeatherServiceImpl implements WeatherService{
	
	/**
	 * Calling EJB
	 */
	@EJB(mappedName = "java:global/appla-ear/appla-ejb/WeatherFacade!com.applaudo.app.facadeLocal.WeatherFacadeLocal")
	private WeatherFacadeLocal weatherFacadeLocalDAO;

	@Override
	public void create(Weather weather) {
		weatherFacadeLocalDAO.create(weather);		
	}

	@Override
	public Weather add(Weather weather) {
		return weatherFacadeLocalDAO.add(weather);
	}

	@Override
	public Weather findById(Integer id) {
		return weatherFacadeLocalDAO.findById(id);
	}

	@Override
	public List<Weather> findAll() {
		return weatherFacadeLocalDAO.findAll();
	}

	@Override
	public List<Weather> findByCity(String city) {
		return weatherFacadeLocalDAO.findByCity(city);
	}

	@Override
	public List<Weather> findByDate(String date) {
		List<Weather> weatherListResponse = new ArrayList<>();
		List<Weather> weatherList = findAll();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
		
		for(Weather weather:weatherList) {
			Date d =weather.getDate_();
			String dateStr = sdf.format(d);
			if(dateStr.equalsIgnoreCase(date)) {
				weatherListResponse.add(weather);
			}
		}
		return weatherListResponse;
	}
}

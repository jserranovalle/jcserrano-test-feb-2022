package com.applaudo.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.applaudo.app.business.WeatherBusinessLogic;
import com.applaudo.app.entidades.Weather;

@Controller
@RequestMapping
public class WeatherApiRestController {
	
	@Autowired
	WeatherBusinessLogic weatherBusinessLogic;
	
	
	@RequestMapping(value = "/weather",  method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> create(@RequestBody Weather weather) {		
		return weatherBusinessLogic.createUser(weather);
	}
	
	
	@RequestMapping(value = "/weather/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> findById(@PathVariable("id") Integer id) {
		return weatherBusinessLogic.findById(id);			
	}
	
	@RequestMapping(value = "/weather", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> getAll(@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "sort", required = false) String sort,
			@RequestParam(value = "date", required = false) String date) {		
		return weatherBusinessLogic.findAll(city,sort,date);
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.applaudo.app.config;




import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
/**
 *
 * @author Juan.serrano
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.applaudo.app.controller"})
public class WebConfig extends WebMvcConfigurerAdapter{

    
    //WebMvcConfigurerAdapter
	
	//To validate uri parameters 
	@Bean
	public MethodValidationPostProcessor methodValidationPostProcessor() {
	         return new MethodValidationPostProcessor();
	}
	

}

package com.applaudo.app.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;



/**
 *
 * @author Juan.serrano
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages =  {"com.applaudo.app.service","com.applaudo.app.business"})
public class BeanContextConfiguration {
    
}

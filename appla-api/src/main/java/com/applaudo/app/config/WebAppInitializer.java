/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.applaudo.app.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


/**
 *
 * @author Juan Serrano
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

    

    // Load spring web configuration
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
       return new String[] { "/" };
    }

	@Override
	protected Class<?>[] getRootConfigClasses() {
		 return new Class[] {BeanContextConfiguration.class};
	}
    
    
}

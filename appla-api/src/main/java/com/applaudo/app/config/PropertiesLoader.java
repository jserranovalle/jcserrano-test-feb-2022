
package com.applaudo.app.config;


import java.io.FileInputStream;
import java.net.URL;
import java.util.Properties;


public class PropertiesLoader {
 

    public PropertiesLoader() {
        load();
    }

    public static PropertiesLoader getInstance() {
        if (instance == null) {
             synchronized(PropertiesLoader.class) {
                if (instance == null) {
                    instance = new PropertiesLoader();
                }
            }
        }
        return instance;
    }

    public Properties load() {
        
        try {
            URL fileUrl =getClass().getResource("/appConfig.properties");
            FileInputStream in = new FileInputStream(fileUrl.getPath());
            properties.load(in);
            in.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return properties;
    }

    public String getProperty(String prop) {
        return properties.getProperty(prop);
    }
    private static PropertiesLoader instance;
    private static Properties properties = new Properties();  
}
